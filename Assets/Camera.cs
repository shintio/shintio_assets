﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Camera : MonoBehaviour
{
	public Transform followUp;
	public Vector3 position;

	private void Start()
	{
		transform.position = followUp.position;
	}

	void Update()
	{
		position = followUp.position;

		position.z = -10f;
		position.x = 0f;

		transform.position = Vector3.Lerp(transform.position, position, 1f * Time.deltaTime);
	}
}