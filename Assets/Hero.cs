﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hero : MonoBehaviour
{
	public float horizontal;

	private Rigidbody2D _rigidBody;

	private void Start()
	{
		_rigidBody = GetComponent<Rigidbody2D>();
		Console.WriteLine(123);
		Debug.Log(1234);
	}

	private void FixedUpdate()
	{
		horizontal = Input.GetAxis("Horizontal");

		_rigidBody.velocity = new Vector2(horizontal * 10f, _rigidBody.velocity.y);
	}

	private void OnCollisionEnter2D(Collision2D other)
	{
		switch (other.gameObject.tag)
		{
			case "Platform":
			{
				_rigidBody.velocity = Vector2.zero;

				_rigidBody.AddForce(transform.up * 20, ForceMode2D.Impulse);
			}
				break;
		}
	}
}